"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers  
from home.views import *
from about.views import *
from photo.views import *
from account.views import *


from django.conf.urls import (handler400, handler403, handler404, handler500)


handler400 = 'error_handler.views.page_not_found'
handler403 = 'error_handler.views.page_not_found'
handler404 = 'error_handler.views.page_not_found'
handler500 = 'error_handler.views.page_not_found'


router = routers.DefaultRouter()  
router.register('all', ShowPost) 
router.register('own', ShowOwnPost)
router.register('user', ShowUser)
router.register('userpost', ShowUserPost, 'UserPost')
router.register('users', ShowUsersInfo)
router.register('home',Home)
router.register('about', About)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('account/', include('account.urls')),
    path('', include('photo.urls')),
    path('post/api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

