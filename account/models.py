from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import Thumbnail, ResizeToFill, Adjust



# Create your models here.
from django.contrib.auth.models import AbstractUser
class User(AbstractUser):
    nickname = models.CharField(max_length=64)
    me = models.TextField()
    facebook = models.CharField(max_length=256, default="")
    instargram = models.CharField(max_length=256, default="")
    youtube = models.CharField(max_length=256, default="")
    image = ProcessedImageField(upload_to = "profile/ori_ori/%Y/%m/%d", processors=[Adjust()], format="JPEG")
    image_thumbnail = ProcessedImageField(
        upload_to = "profile/thum/%Y/%m/%d",
        processors=[Thumbnail(600, 600)],
        format='JPEG',
        options={'quality':60}
    )
    


    





class EmailActive(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='account_email_pass')
    pass_code = models.CharField(max_length=32, default="passCode")