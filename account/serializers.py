from rest_framework import serializers
from account.models import User
from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta: 
        model = User
        fields = ('nickname', 'image_thumbnail', 'me', 'facebook', 'instargram', 'youtube')


class UploadUserSerializer(serializers.ModelSerializer):
    class Meta: 
        model = User
        fields = ('nickname', 'image_thumbnail')