from django import forms
from django.contrib.auth.models import User
from .models import *

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['nickname', 'username', 'first_name', 'last_name', 'email', 'password', 'image', 'image_thumbnail','me',
                    'facebook', 'instargram', 'youtube']
    
    # def save(self, commit=True):
    #     User.is_active = False
    #     User.save(commit)

class EmailPassForm(forms.ModelForm):
    class Meta:
        model = EmailActive
        fields = ['user', 'pass_code']


class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
            }
        )
    )