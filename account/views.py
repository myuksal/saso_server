from django.shortcuts import render, HttpResponse, redirect

import random

from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from django.core.mail import send_mail
from django.template import Context
from django.template.loader import get_template, render_to_string

from django.contrib.auth.hashers import check_password
from django.contrib.auth import (
    authenticate,
    login as django_login,
    logout as django_logout,
)

from django.contrib.auth.views import LoginView, LogoutView

from rest_framework import viewsets, generics, pagination
from .models import *
from .forms import *
from .serializers import *



def signup(request):
    context = {}
    if request.method == "POST":
        if(request.POST['password'] != request.POST['password2']):
            context.update({"passwordValid":"비밀번호가 일치하지 않습니다."})
        else:
            form = UserForm(request.POST, request.FILES)
            request.FILES.update({"image_thumbnail":request.FILES['image']})
            if form.is_valid():
                new_user = User.objects.create_user(**form.cleaned_data)
                chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
                code = ""
                for i in range(0,30):
                    code = code + random.choice(chars)
                msg_html = render_to_string('account/account_pass.html', {'username': request.POST.get('username'), "code":code})
                
                send_mail(
                    '테스트 타이틀',
                    "안녕하세요 멱살샵입니다. 본인확인을 위해 이메일을 발송해드렷습니다.",
                    '멱살샵',
                    [request.POST.get('email')],
                    html_message=msg_html,
                )

                pass_user = User.objects.get(username=request.POST.get('username'))

                new_pass = EmailActive(user=pass_user, pass_code=code).save()


                return render(request, 'account/signup_ready.html', context)
            else:
                context.update({'form': form})
                return render(request, 'account/adduser.html', context)
    form = UserForm()
    context.update({'form': form})
    return render(request, 'account/adduser.html', context)

def signpass(request, slug):
    
    context = {}
    pass_user = EmailActive.objects.get(pass_code = slug)
    user = User.objects.filter(username=pass_user.user)
    user.update(is_active=True) 
    context.update({"pass_user":list(user.values())[0]})
    return render(request, 'account/pass_done.html', context)


def id_check(request):
    try:
        is_it = User.objects.get(username=request.POST.get("username"))
        return HttpResponse("no")
    except:
        return HttpResponse("go")



login = LoginView.as_view(template_name='account/login.html')
logout = LogoutView.as_view()


class ShowUser(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(is_staff=True)
    serializer_class = UserSerializer
    http_method_names = ['get',]


class ShowUsersInfo(viewsets.ModelViewSet):
    queryset = User.objects.all().filter(is_staff=True)
    serializer_class = UserSerializer
    http_method_names = ['get',]