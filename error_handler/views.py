from django.shortcuts import render, render_to_response

# Create your views here.



def bad_request(request):
    response = render_to_response('error/404.html', {})
    response.status_code = 404
    return response
def permission_denied(request):
    response = render_to_response('error/404.html', {})
    response.status_code = 404
    return response
def page_not_found(request):
    response = render_to_response('error/404.html', {})
    response.status_code = 404
    return response
def server_error(request):
    response = render_to_response('error/404.html', {})
    response.status_code = 404
    return response