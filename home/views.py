from django.shortcuts import render
from rest_framework import viewsets, generics, pagination
from django.contrib.auth.hashers import check_password
from .models import *
from .serializers import *


class Home(viewsets.ModelViewSet):
    queryset = Home.objects.all().filter(enable=True).order_by('id')
    serializer_class = HomeSerializer
    http_method_names = ['get',]