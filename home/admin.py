from django.contrib import admin
from .models import *

@admin.register(Home)
class HomeAdmin(admin.ModelAdmin):
    list_display = ('main_title', 'sub_title', 'color')
