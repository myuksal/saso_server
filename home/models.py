from django.db import models

from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import Thumbnail, ResizeToFit, Adjust


class Home(models.Model):
    image = ProcessedImageField(
        # source="photo",
        upload_to='home_image/%Y/%m/%d', 
        processors=[Adjust()],
        format='JPEG'
    )
    color = models.CharField(max_length=30, default="white")
    main_title = models.CharField(max_length=30, default="white")
    sub_title = models.CharField(max_length=100, default="white")
    enable = models.BooleanField(default=True)