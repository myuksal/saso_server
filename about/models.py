from django.db import models

from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import Thumbnail, ResizeToFit, Adjust


class About(models.Model):
    order = models.IntegerField(default=0)
    image = ProcessedImageField(
        # source="photo",
        upload_to='about_image/%Y/%m/%d', 
        processors=[Adjust()],
        format='JPEG'
    )
    is_contact = models.BooleanField(default=False, help_text="연락처라면 체크")