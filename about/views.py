from django.shortcuts import render
from rest_framework import viewsets, generics, pagination
from django.contrib.auth.hashers import check_password
from .models import *
from .serializers import *

# Create your views here.

class About(viewsets.ModelViewSet):
    queryset = About.objects.all()
    serializer_class = AboutSerializer
    http_method_names = ['get',]