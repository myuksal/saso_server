from django.contrib import admin
from .models import *

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('index', 'user', 'iso', 'shutter_speed', 'aperture')




