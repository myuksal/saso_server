from django import forms
from django.contrib.auth.models import User
from .models import *

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['nickname', 'username', 'first_name', 'last_name', 'email', 'image', 'image_thumbnail','me',
                    'facebook', 'instargram', 'youtube']

class UploadPostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields=['iso', 'shutter_speed', 'aperture', 
                'content',]
    
class UpdatePostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields=['iso', 'shutter_speed', 'aperture', 'content',]
    
