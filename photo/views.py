from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from rest_framework import viewsets, generics, pagination
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib.auth.hashers import check_password
from .models import *
from .forms import *
from .serializers import *
from account.serializers import *
from django.views.decorators.csrf import csrf_exempt


def index(request):
	return render(request, 'index.html', {})


def upload_post(request):
	if not request.user.is_staff:
		return HttpResponse("스태프 권한이 없습니다.")
	if request.method == 'POST':
		form = UploadPostForm(request.POST, request.FILES)	
		if form.is_valid():
			upload = form.save(commit=False)
			upload.user= request.user
			upload.photo = request.FILES.dict()['photo']
			upload.photo_thumbnail = request.FILES['photo']
			upload.photo_show = request.FILES['photo']
			upload.save()
			return render(request, 'photo/upload_end.html', {})
	form = UploadPostForm()
	return render(request, 'photo/upload_post.html', {"form":form})


def me(request):
	context = {}
	if not request.method == 'POST':
		context.update({"form":UserForm})
		return render(request, "photo/editme.html", context)
	user = get_object_or_404(User, pk = request.user.id)
	if not check_password(request.POST['password'], user.password):
		context.update({"form":UserForm, "pass_error":"비밀번호가 틀렷습니다"})
	else:
		form = UserForm(request.POST, request.FILES, instance=user)
		if form.is_valid():
			update = form.save(commit=False)
			update.image_thumbnail = request.FILES.dict()['image']
			update.save()
			context.update({"sucsses":"수정 완료", "end_color":"rgb(49,170,49)"})
		else :
			context.update({"sucsses":"수정 실패", "end_color":"rgb(170,49,49)"})	
			
	user = get_object_or_404(User, pk = request.user.id)
	context.update({"form":UserForm(instance=user)})
	return render(request, "photo/editme.html", context)


def me_all(request):
	if not request.user.is_staff:
		return HttpResponse("스태프 권한이 없습니다.")
	post_list = Post.objects.all().filter(user = request.user).filter(enable=True).order_by('-index')
	return me_content(request, post_list)




def me_content(request, post_list):
	paginator = Paginator(post_list, 10)
	page = request.GET.get('page')
	contacts = paginator.get_page(page)
	return render(request, "photo/me_all.html", {'contacts':contacts})


def me_update(request):

	if not request.method == 'POST':
		return HttpResponse("응 아니야")
	index = request.POST.get('index')
	post = get_object_or_404(Post ,pk = index)
	if post.index!= int(index):
		return HttpResponse("더더욱 아니야")
	form = UpdatePostForm(request.POST, instance=post)
	if form.is_valid():
		form.save()
	return redirect('me_all')



def me_delete(request):
	index = request.GET.get('del')
	print(index)
	post = get_object_or_404(Post ,pk = index)
	if post.index!= int(index):
		return HttpResponse("너꺼 아니야")
	post.enable = False
	post.save()

	return redirect('me_all')




class ShowPostSetPagination(pagination.PageNumberPagination):
	page_size = 10
	page_size_query_param = 'page_size'
	max_page_size = 10000
	http_method_names = ['get']


class ShowPost(viewsets.ModelViewSet):
# class ShowPost(generics.ListAPIView):
	queryset = Post.objects.all().filter(enable=True).order_by('-index')
	serializer_class = PostSerializer
	pagination_class = ShowPostSetPagination
	http_method_names = ['get']
class ShowOwnPost(viewsets.ModelViewSet):
	queryset = Post.objects.all().filter(enable=True).order_by('-index')
	serializer_class = PostSerializer

	def get_queryset(self):
		id = self.request.query_params.get('id', None)
		queryset = Post.objects.all().filter(enable=True).order_by('-index')
		if id is not None:
			queryset = queryset.filter(index=id)
			return queryset
	http_method_names = ['get']


class ShowUserPost(viewsets.ModelViewSet):
	
	serializer_class = PostSerializer
	pagination_class = ShowPostSetPagination
	http_method_names = ['get']

	def get_queryset(self):
		queryset = Post.objects.all().filter(enable=True).order_by('-index')
		nickname = self.request.query_params.get('nickname', None)
		if nickname is not None:
			user = get_object_or_404(User, nickname=nickname)
			queryset = queryset.filter(user=user)
			return queryset










