from rest_framework import serializers
from account.models import User
from account.serializers import UserSerializer, UploadUserSerializer
from .models import *




class UploadUserSerializer(serializers.ModelSerializer):
    class Meta: 
        model = User
        fields = ('nickname', 'image_thumbnail')

class PostSerializer(serializers.ModelSerializer):
    user = UploadUserSerializer(read_only=True)
    upload_time = serializers.DateTimeField(format='%Y-%m-%d')

    class Meta:
        model = Post
        fields = ('index', 'upload_time', 'user'
                ,'iso', 'shutter_speed', 'aperture', 'content'
                ,'photo_thumbnail', 'photo_show')
    
