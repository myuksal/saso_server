from django.db import models
from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import Thumbnail, ResizeToFit, Adjust

from account.models import User




# Create your models here.


class Post(models.Model):
    index = models.BigAutoField(primary_key=True)
    upload_time = models.DateTimeField(auto_now=True ,help_text="등록 날짜")
    user = models.ForeignKey(User, related_name='users', on_delete=models.CASCADE)
    iso = models.IntegerField(default=0)
    shutter_speed = models.CharField(max_length=10)
    aperture = models.CharField(max_length=50, default="")

    content = models.TextField()

    # photo = models.ImageField(upload_to = "photo/ori_ori/%Y/%m/%d")
    photo = ProcessedImageField(
        # source="photo",
        upload_to='photo/ori_ori/%Y/%m/%d', 
        processors=[Adjust()],
        format='JPEG'
    )
    photo_thumbnail = ProcessedImageField(
        upload_to = "photo/thum/%Y/%m/%d",
        # source="photo",
        processors=[ResizeToFit(600)],
        format='JPEG',
        options={'quality':60}
    )
    photo_show = ProcessedImageField(
        # source="photo",
        upload_to='photo/show/%Y/%m/%d', 
        processors=[ResizeToFit(1800)],
        format='JPEG'
    )
    enable = models.BooleanField(default=True)
    class Meta:
        unique_together = ('index', 'upload_time')
    








