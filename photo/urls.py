"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import *

urlpatterns = [
    path('upload', login_required()(upload_post), name='upload'),
    path('me', login_required()(me), name="me"),                            path('me/', login_required()(me), name="me"),
    path('me/all', login_required()(me_all), name="me_all"),                path('me/all/', login_required()(me_all), name="me_all"),
    path('me/update', login_required()(me_update), name="me_update"),       path('me/update/', login_required()(me_update), name="me_update"),
    path('me/delete', login_required()(me_delete)),                         path('me/delete/', login_required()(me_delete)),
    path('', index, name="index")
]
